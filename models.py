"""
This file defines the database models
"""

import datetime
from .common import db, Field, auth
from pydal.validators import *


def get_user_email():
    return auth.current_user.get('email') if auth.current_user else None

def get_time():
    return datetime.datetime.utcnow()


### Define your table below
#
# db.define_table('thing', Field('name'))
#
## always commit your models to avoid problems later

db.define_table(
    'contacts',
    ### TODO: define the fields that are in the json.
    Field('first_name', requires=IS_NOT_EMPTY()),
    Field('last_name', requires=IS_NOT_EMPTY()),
    Field('user_email', default=get_user_email),
)

db.contacts.id.readable = db.contacts.id.writable = False
db.contacts.user_email.readable = db.contacts.user_email.writable = False

db.define_table(
    'phone',
    ### TODO: define the fields that are in the json.
    Field('contact_id', 'reference contacts'),
    Field('phone_number', requires=IS_NOT_EMPTY()),
    Field('phone_name', requires=IS_NOT_EMPTY()),
)
db.phone.id.readable = db.phone.id.writable = False
db.phone.contact_id.readable = db.phone.contact_id.writable = False

db.commit()
