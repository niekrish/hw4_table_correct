"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

from py4web import action, request, abort, redirect, URL
from yatl.helpers import A

from py4web.utils.form import Form, FormStyleBulma
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash
from py4web.utils.url_signer import URLSigner
from .models import get_user_email

url_signer = URLSigner(session)


@action('index')
@action.uses(db, auth.user, 'index.html')
def index():
    ## TODO: Show to each logged in user the birds they have seen with their count.
    # The table must have an edit button to edit a row, and also, a +1 button to increase the count
    # by 1 (this needs to be protected by a signed URL).
    # On top of the table there is a button to insert a new bird.
    rows = db(db.contacts.user_email == get_user_email()).select()
    my_list = db(db.phone.contact_id == db.contacts.id).select().as_list()
    #my_list_first_name = db(db.phone.contact_id == db.contacts.id).select('first_name').as_list()
    #my_list_last_name = db(db.phone.contact_id == db.contacts.id).select('last_name').as_list()
    #my_list_phone = db(db.phone.contact_id == db.contacts.id).select('phone_number').as_list()
    #new_dict = {'name1': {}, 'name2': {},  'phones': {}}
    for elem in my_list:
        my_string = elem['phone']['phone_number'] + " (" + elem['phone']['phone_name'] + "),"
        elem['phone']['phone_number'] = my_string
    #for row in my_list:
     #   if row['contact']['first_name'] is not in perso
        #elem["phone_number"] = my_string
    return dict(
                rows=rows,
                url_signer=url_signer,
                my_list=my_list,
                my_string=my_string,
                #new_dict=new_dict,
                #my_list_first_name=my_list_first_name,
                #my_list_last_name=my_list_last_name,
                #my_list_phone=my_list_phone
               )

@action('add_contact', method = ["GET", "POST"])
@action.uses(db, session, auth.user, 'add_contact.html')
def add_contact():
    form = Form(db.contacts, csrf_session=session, formstyle= FormStyleBulma)
    if form.accepted:
        redirect(URL('index'))
    return dict(form=form)

@action('edit_contact/<id>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'edit_contact.html', url_signer.verify())
def edit_contact(id=None):
    assert id is not None
    p = db.contacts[id]
    if p is None:
        redirect(URL('index'))
    form = Form(db.contacts, record=p, deletable=False, csrf_session=session, formstyle= FormStyleBulma)
    if form.accepted:
        redirect(URL('index'))
    return dict(form=form,
                url_signer=url_signer
                )

@action('delete_contact/<id>')
@action.uses(db, session, auth.user, url_signer.verify())
def delete_contact(id=None):
    assert id is not None
    db(db.contacts.id == id).delete()
    redirect(URL('index'))


@action('edit_phone/<contact_id:int>')
@action.uses(db, session, auth.user, 'edit_phone.html', url_signer.verify())
def edit_phone(contact_id=None):
    rows2 = db(db.phone.contact_id == contact_id).select()
    p = db.contacts[contact_id]
    name_string = "Phone Numbers for " + p.first_name + " " + p.last_name
    return dict(rows2=rows2,
                contact_id=contact_id,
                name_string=name_string,
                url_signer=url_signer)

@action('add_phone/<contact_id:int>', method = ["GET", "POST"])
@action.uses(db, session, auth.user, 'add_phone.html')
def add_phone(contact_id=None):
    form = Form(db.phone, csrf_session=session, formstyle= FormStyleBulma, dbio=False)
    if form.accepted:
        db.phone.insert(contact_id=contact_id, phone_name=form.vars["phone_name"], phone_number=form.vars["phone_number"])
        redirect(URL('edit_phone', contact_id, signer=url_signer))
    return dict(form=form)

@action('back')
@action.uses(db, session, auth.user)
def back():
    redirect(URL('index'))
    return dict()

@action('edit_phone_numbers/<contact_id>/<phone_id>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'edit_phone_numbers.html', url_signer.verify())
def edit_phone_numbers(contact_id, phone_id=None):
    assert phone_id is not None
    p = db.phone[phone_id]
    if p is None:
        redirect(URL('edit_phone', contact_id))
    d = db.contacts[contact_id]
    name_string = "Phone Numbers for " + d.first_name + " " + d.last_name
    form = Form(db.phone, record=p, deletable=False, csrf_session=session, formstyle= FormStyleBulma)
    if form.accepted:
        redirect(URL('edit_phone', p.contact_id, signer=url_signer))
    return dict(form=form,
                url_signer=url_signer,
                name_string=name_string
                )

@action('delete_phone/<phone_id>')
@action.uses(db, session, auth.user, url_signer.verify())
def delete_phone(phone_id=None):
    assert phone_id is not None
    p = db.phone[phone_id]
    db(db.phone.id == phone_id).delete()
    redirect(URL('edit_phone', p.contact_id, signer=url_signer))
